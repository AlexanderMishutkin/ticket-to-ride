package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Objects;

public class Card implements Serializable {
    Integer colorId;


    public Card(Integer colorId) {
        this.colorId = colorId;
    }

    public Card() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(colorId, card.colorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(colorId);
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("colorId", colorId)
                .toString();
    }
}
