package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Objects;

public class Auto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String number;

    public Auto(String number) {
        this.number = number;
    }

    public Auto() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Auto(number=" + this.number + ")";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auto auto = (Auto) o;
        return Objects.equals(number, auto.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
