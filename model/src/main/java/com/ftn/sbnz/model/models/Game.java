package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Objects;

public class Game {
    List<User> users = List.of();
    List<Step> possibleSteps = List.of();
    Step currentStep;
    Boolean isFinished = false;
    Map map;
    List<Card> cardsInDeck = List.of();
    List<Card> openCards = List.of();
    List<Task> tasksInDeck = List.of();
    User currentUser;


    public Game() {
    }

    public Game(List<User> users, List<Step> possibleSteps, Step currentStep, Boolean isFinished, Map map, List<Card> cardsInDeck, List<Card> openCards, List<Task> tasksInDeck, User currentUser) {
        this.users = users;
        this.possibleSteps = possibleSteps;
        this.currentStep = currentStep;
        this.isFinished = isFinished != null && isFinished;
        this.map = map;
        this.cardsInDeck = cardsInDeck;
        this.openCards = openCards;
        this.tasksInDeck = tasksInDeck;
        this.currentUser = currentUser;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Step> getPossibleSteps() {
        return possibleSteps;
    }

    public void setPossibleSteps(List<Step> possibleSteps) {
        this.possibleSteps = possibleSteps;
    }

    public Step getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(Step currentStep) {
        this.currentStep = currentStep;
    }

    public Boolean getFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished != null && finished;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public List<Card> getCardsInDeck() {
        return cardsInDeck;
    }

    public void setCardsInDeck(List<Card> cardsInDeck) {
        this.cardsInDeck = cardsInDeck;
    }

    public List<Card> getOpenCards() {
        return openCards;
    }

    public void setOpenCards(List<Card> openCards) {
        this.openCards = openCards;
    }

    public List<Task> getTasksInDeck() {
        return tasksInDeck;
    }

    public void setTasksInDeck(List<Task> tasksInDeck) {
        this.tasksInDeck = tasksInDeck;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(users, game.users) && Objects.equals(possibleSteps, game.possibleSteps) && Objects.equals(currentStep, game.currentStep) && Objects.equals(isFinished, game.isFinished) && Objects.equals(map, game.map) && Objects.equals(cardsInDeck, game.cardsInDeck) && Objects.equals(openCards, game.openCards) && Objects.equals(tasksInDeck, game.tasksInDeck) && Objects.equals(currentUser, game.currentUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(users, possibleSteps, currentStep, isFinished, map, cardsInDeck, openCards, tasksInDeck, currentUser);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("users", users)
                .append("possibleSteps", possibleSteps)
                .append("currentStep", currentStep)
                .append("isFinished", isFinished)
                .append("map", map)
                .append("cardsInDeck", cardsInDeck)
                .append("openCards", openCards)
                .append("tasksInDeck", tasksInDeck)
                .append("currentUser", currentUser)
                .toString();
    }
}
