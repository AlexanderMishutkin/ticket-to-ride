package com.ftn.sbnz.model.events;

import com.ftn.sbnz.model.models.Auto;
import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

import java.io.Serializable;

@Role(Role.Type.EVENT)
@Expires("10m")
public class DangerousEvent implements Serializable {
    private static final long serialVersionUID = 1L;

    public DangerousEvent() {
    }

    public DangerousEvent(Auto auto, Reason reason) {
        this.auto = auto;
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "DangerousEvent(reason=" + this.reason + ", auto=" + this.auto.getNumber() + ")";
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public enum Reason {
        HIGH_ENGINE_TEMP, LOW_WHEEL_PRESSURE, LOW_OIL_LEVEL
    };

    private Auto auto;
    private Reason reason;
}
