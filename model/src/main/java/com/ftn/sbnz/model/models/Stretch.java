package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Stretch {
    PotentialStretch potentialStretch;
    User owner;

    public Stretch(PotentialStretch potentialStretch, User owner) {
        this.potentialStretch = potentialStretch;
        this.owner = owner;
    }

    public Stretch() {
    }

    public PotentialStretch getPotentialStretch() {
        return potentialStretch;
    }

    public void setPotentialStretch(PotentialStretch potentialStretch) {
        this.potentialStretch = potentialStretch;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("potentialStretch", potentialStretch)
                .append("owner", owner)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stretch stretch = (Stretch) o;
        return Objects.equals(potentialStretch, stretch.potentialStretch) && Objects.equals(owner, stretch.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(potentialStretch, owner);
    }
}
