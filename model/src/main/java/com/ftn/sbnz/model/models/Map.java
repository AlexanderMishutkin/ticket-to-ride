package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Objects;

public class Map {
    List<City> cities = List.of();
    List<PotentialStretch> potentialStretches = List.of();
    List<Stretch> stretches = List.of();
    List<Station> stations = List.of();

    public Map() {
    }


    public Map(List<City> cities, List<PotentialStretch> potentialStretches, List<Stretch> stretches, List<Station> stations) {
        this.cities = cities;
        this.potentialStretches = potentialStretches;
        this.stretches = stretches;
        this.stations = stations;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<PotentialStretch> getPotentialStretches() {
        return potentialStretches;
    }

    public void setPotentialStretches(List<PotentialStretch> potentialStretches) {
        this.potentialStretches = potentialStretches;
    }

    public List<Stretch> getStretches() {
        return stretches;
    }

    public void setStretches(List<Stretch> stretches) {
        this.stretches = stretches;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Map map = (Map) o;
        return Objects.equals(cities, map.cities) && Objects.equals(potentialStretches, map.potentialStretches) && Objects.equals(stretches, map.stretches) && Objects.equals(stations, map.stations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cities, potentialStretches, stretches, stations);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("cities", cities)
                .append("potentialStretches", potentialStretches)
                .append("stretches", stretches)
                .append("stations", stations)
                .toString();
    }
}
