package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.kie.api.definition.type.Position;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class User implements Serializable {
    @Position(0)
    private String name;
    @Position(1)
    private String parentName;

    @Position(2)
    private List<Card> cards = List.of();
    @Position(3)
    private List<Task> tasks = List.of();

    @Position(4)
    private Integer score = 0;

    @Position(5)
    private Integer trainsLeft = 0;

    public User(String name, String parentName) {
        this.name = name;
        this.parentName = parentName;
    }

    public User(String name, String parentName, List<Card> cards, List<Task> tasks) {
        this.name = name;
        this.parentName = parentName;
        this.cards = cards;
        this.tasks = tasks;
    }

    public User() {
    }

    public User(String name, String parentName, List<Card> cards, List<Task> tasks, Integer score) {
        this.name = name;
        this.parentName = parentName;
        this.cards = cards;
        this.tasks = tasks;
        this.score = score;
    }

    public User(String name, String parentName, List<Card> cards, List<Task> tasks, Integer score, Integer trainsLeft) {
        this.name = name;
        this.parentName = parentName;
        this.cards = cards;
        this.tasks = tasks;
        this.score = score;
        this.trainsLeft = trainsLeft;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getTrainsLeft() {
        return trainsLeft;
    }

    public void setTrainsLeft(Integer trainsLeft) {
        this.trainsLeft = trainsLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) && Objects.equals(parentName, user.parentName) && Objects.equals(cards, user.cards) && Objects.equals(tasks, user.tasks) && Objects.equals(score, user.score) && Objects.equals(trainsLeft, user.trainsLeft);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parentName, cards, tasks, score, trainsLeft);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("parentName", parentName)
                .append("cards", cards)
                .append("tasks", tasks)
                .append("score", score)
                .append("trainsLeft", trainsLeft)
                .toString();
    }
}
