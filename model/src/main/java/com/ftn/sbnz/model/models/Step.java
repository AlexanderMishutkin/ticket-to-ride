package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Objects;

public class Step {
    public static enum Type {
        NOT_STARTED,
        TAKE_OPEN_JOKER,
        TAKE_FIRST_OPEN_CARD,
        TAKE_SECOND_OPEN_CARD,
        TAKE_FIRST_CLOSED_CARD,
        TAKE_SECOND_CLOSED_CARD,
        TAKE_THREE_TASKS,
        RETURN_TASKS,
        BUILD_STRETCH,
        BUILD_STATION,
        GAME_OVER
    }

    List<Card> relatedCards = List.of();
    City relatedCity;
    Stretch relatedStretch;
    List<Task> relatedTasks = List.of();
    Type type = Type.NOT_STARTED;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public Step(Object relatedCards, City relatedCity, Stretch relatedStretch, List<Task> relatedTasks,
                Type type) {
        this.relatedCards = (List<Card>) relatedCards;
        this.relatedCity = relatedCity;
        this.relatedStretch = relatedStretch;
        this.relatedTasks = relatedTasks;
        this.type = type;
    }


    public Step(List<Card> relatedCards, City relatedCity, Stretch relatedStretch, List<Task> relatedTasks,
                Type type) {
        this.relatedCards = relatedCards;
        this.relatedCity = relatedCity;
        this.relatedStretch = relatedStretch;
        this.relatedTasks = relatedTasks;
        this.type = type;
    }

    public Step() {
    }

    public List<Card> getRelatedCards() {
        return relatedCards;
    }

    public void setRelatedCards(List<Card> relatedCards) {
        this.relatedCards = relatedCards;
    }

    public City getRelatedCity() {
        return relatedCity;
    }

    public void setRelatedCity(City relatedCity) {
        this.relatedCity = relatedCity;
    }

    public Stretch getRelatedStretch() {
        return relatedStretch;
    }

    public void setRelatedStretch(Stretch relatedStretch) {
        this.relatedStretch = relatedStretch;
    }

    public List<Task> getRelatedTasks() {
        return relatedTasks;
    }

    public void setRelatedTasks(List<Task> relatedTasks) {
        this.relatedTasks = relatedTasks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("relatedCards", relatedCards)
                .append("relatedCity", relatedCity)
                .append("relatedStretch", relatedStretch)
                .append("relatedTasks", relatedTasks)
                .append("type", type)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Step step = (Step) o;
        return Objects.equals(relatedCards, step.relatedCards) && Objects.equals(relatedCity, step.relatedCity) && Objects.equals(relatedStretch, step.relatedStretch) && Objects.equals(relatedTasks, step.relatedTasks) && type == step.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(relatedCards, relatedCity, relatedStretch, relatedTasks, type);
    }
}
