package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Task {
    Integer award;
    City firstCity;
    City secondCity;

    public Task() {
    }

    public Task(Integer award, City firstCity, City secondCity) {
        this.award = award;
        this.firstCity = firstCity;
        this.secondCity = secondCity;
    }

    public Integer getAward() {
        return award;
    }

    public void setAward(Integer award) {
        this.award = award;
    }

    public City getFirstCity() {
        return firstCity;
    }

    public void setFirstCity(City firstCity) {
        this.firstCity = firstCity;
    }

    public City getSecondCity() {
        return secondCity;
    }

    public void setSecondCity(City secondCity) {
        this.secondCity = secondCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(award, task.award) && Objects.equals(firstCity, task.firstCity) && Objects.equals(secondCity, task.secondCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(award, firstCity, secondCity);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("award", award)
                .append("firstCity", firstCity)
                .append("secondCity", secondCity)
                .toString();
    }
}
