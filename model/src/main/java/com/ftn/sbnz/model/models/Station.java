package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Station {
    City city;
    User owner;

    public Station(City city, User owner) {
        this.city = city;
        this.owner = owner;
    }

    public Station() {
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("city", city)
                .append("owner", owner)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return Objects.equals(city, station.city) && Objects.equals(owner, station.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, owner);
    }
}
