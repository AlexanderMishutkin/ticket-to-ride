package com.ftn.sbnz.model.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class PotentialStretch {
    Integer length;
    Integer jokersNeeded;
    Integer colorId;
    Boolean isTunnel;
    City firstCity;
    City secondCity;

    public PotentialStretch() {
    }

    public PotentialStretch(Integer length, Integer jokersNeeded, Integer colorId, Boolean isTunnel, City firstCity, City secondCity) {
        this.length = length;
        this.jokersNeeded = jokersNeeded;
        this.colorId = colorId;
        this.isTunnel = isTunnel;
        this.firstCity = firstCity;
        this.secondCity = secondCity;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getJokersNeeded() {
        return jokersNeeded;
    }

    public void setJokersNeeded(Integer jokersNeeded) {
        this.jokersNeeded = jokersNeeded;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public Boolean getTunnel() {
        return isTunnel;
    }

    public void setTunnel(Boolean tunnel) {
        isTunnel = tunnel;
    }

    public City getFirstCity() {
        return firstCity;
    }

    public void setFirstCity(City firstCity) {
        this.firstCity = firstCity;
    }

    public City getSecondCity() {
        return secondCity;
    }

    public void setSecondCity(City secondCity) {
        this.secondCity = secondCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PotentialStretch that = (PotentialStretch) o;
        return Objects.equals(length, that.length) && Objects.equals(jokersNeeded, that.jokersNeeded) && Objects.equals(colorId, that.colorId) && Objects.equals(isTunnel, that.isTunnel) && Objects.equals(firstCity, that.firstCity) && Objects.equals(secondCity, that.secondCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, jokersNeeded, colorId, isTunnel, firstCity, secondCity);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("length", length)
                .append("jokersNeeded", jokersNeeded)
                .append("colorId", colorId)
                .append("isTunnel", isTunnel)
                .append("firstCity", firstCity)
                .append("secondCity", secondCity)
                .toString();
    }
}
