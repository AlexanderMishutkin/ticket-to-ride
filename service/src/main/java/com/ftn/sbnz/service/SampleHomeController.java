package com.ftn.sbnz.service;

import com.ftn.sbnz.model.models.*;
import com.ftn.sbnz.model.models.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.*;

@RestController
@RequestMapping("/")
public class SampleHomeController {
	private KieServices ks;
	private KieContainer kContainer;
	private KieSession ksession;
	private KieSession ksession2;
	private Integer maxColor = 9;
	private Integer cards = 12;
	private Integer cardsPerPlayer = 6;
	private Integer tasks = 30;
	private Integer tasksPerPlayer = 3;
	private Integer trains = 10;
	private FactHandle gameHandle;
	private FactHandle gameHandle2;

	public void setUp() throws IOException {
		this.ks = KieServices.Factory.get();
		this.kContainer = ks.getKieClasspathContainer();
		this.ksession = kContainer.newKieSession("bwKsession");
		System.out.println(this.ksession);
		System.out.println(ksession);


		InputStream template = new FileInputStream("D:/source/repos/java/ticket-to-ride/kjar/src/main/resources/rules/backward/backward.drt");
		InputStream data = new FileInputStream("D:/source/repos/java/ticket-to-ride/kjar/src/main/resources/data/scoring.csv");
		Reader dataReader = new InputStreamReader(data);

		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(dataReader);
		ArrayList<HashMap<String, Object>> data1 = new ArrayList<>();

		for (CSVRecord record : records) {
			HashMap<String, Object> row = new HashMap<>();
			for (String header : record.toMap().keySet()) {
				row.put(header, record.get(header));
			}
			data1.add(row);
		}

		ObjectDataCompiler compiler = new ObjectDataCompiler();
		String drl = compiler.compile(data1, template);
		ksession2 = this.createKieSessionFromDRL(drl);
	}

	private Game game;
	private String rawMap;

	@RequestMapping(value = "/game/stop", method = RequestMethod.POST)
	public void stopGame() {
		game = null;
		rawMap = null;
	}

	@RequestMapping(value = "/game", method = RequestMethod.GET, produces = "application/json")
	public Game getGame() {
		if (game == null) {
			return null;
		}
		return game;
	}

	@RequestMapping(value = "/map", method = RequestMethod.GET, produces = "application/json")
	public String getMap() {
		if (game == null) {
			return null;
		}
		return rawMap;
	}

	@RequestMapping(value = "/game", method = RequestMethod.POST, produces = "application/json")
	public Game createGame( @RequestBody(required = true) String map) throws IOException {
		game = new Game();
		game.setMap(MapLoader.fromString(map));
		rawMap = map;
		return game;
	}

	@RequestMapping(value = "/step", method = RequestMethod.POST, produces = "application/json")
	public String step(@RequestBody(required = true) Step step) {
		game.setCurrentStep(step);
		ksession.update(gameHandle, game);
		ksession2.update(gameHandle2, game);
		ksession2.fireAllRules();
		ksession.fireAllRules();
		return "ok";
	}

	@RequestMapping(value = "/player", method = RequestMethod.POST, produces = "application/json")
	public User createGame( @RequestBody(required = true) User user) throws IOException {
		// Users are list (ты глупый)
		List<User> oldUsers = game.getUsers();

		// Creating a new list to hold old users along with the new user
		List<User> allUsers = new ArrayList<>();
		if (oldUsers != null) {
			allUsers.addAll(oldUsers);
		}
		allUsers.add(user);
		game.setUsers(allUsers);
		return user;
	}

	private List<Card> generateShuffledDeck() {
		List<Card> deck = new ArrayList<>();
		for (int i = 0; i < maxColor; i++) {
			for (int j = 0; j < cards; j++) {
				deck.add(new Card(i));
			}
		}
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));
		return deck;
	}

	private List<Task> generateTasks() {
		List<Task> tasks = new ArrayList<>();
		List<City> cities = game.getMap().getCities();
		Random random = new Random();

		for (int i = 0; i < this.tasks; i++) {
			City city1 = cities.get(random.nextInt(cities.size()));
			City city2 = cities.get(random.nextInt(cities.size()));
			while (city1.equals(city2)) {
				city2 = cities.get(random.nextInt(cities.size()));
			}
			int award = random.nextInt(10) + 1; // assuming award is between 1 and 100
			tasks.add(new Task(award, city1, city2));
		}
		return tasks;
	}

	private List<Card> takeTopNFromDeck(Game game, int n) {
		List<Card> cards = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			cards.add(game.getCardsInDeck().get(i));
		}
		game.setCardsInDeck(game.getCardsInDeck().subList(n, game.getCardsInDeck().size()));
		return cards;
	}

	private List<Task> takeTopNFromDeckT(Game game, int n) {
		List<Task> tasks = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			tasks.add(game.getTasksInDeck().get(i));
		}
		game.setTasksInDeck(game.getTasksInDeck().subList(n, game.getTasksInDeck().size()));
		return tasks;
	}

	@RequestMapping(value = "/game/start", method = RequestMethod.POST, produces = "application/json")
	public Game startGame() throws IOException {
		this.setUp();

		game.setCurrentUser(game.getUsers().get(0));
		game.setCardsInDeck(
			generateShuffledDeck()
		);
		game.setTasksInDeck(
			generateTasks()
		);
		game.setOpenCards(
			takeTopNFromDeck(game, 5)
		);

		for (User user : game.getUsers()) {
			user.setCards(
				takeTopNFromDeck(game, cardsPerPlayer)
			);
			user.setTasks(
				takeTopNFromDeckT(game, tasksPerPlayer)
			);
			user.setTrainsLeft(trains);
		}

		gameHandle = ksession.insert(game);
		gameHandle2 = ksession2.insert(game);
		ksession.fireAllRules();
		return game;
	}

	@GetMapping
	public ResponseEntity<Map> index() throws IOException {
		this.setUp();

		String currentDirectory = System.getProperty("user.dir");
		System.out.println("Current working directory: " + currentDirectory);

		Map map = MapLoader.loadMap("service/src/main/resources/map.json");
		return ResponseEntity.ok(map);
	}

	private KieSession createKieSessionFromDRL(String drl){
		System.out.println(drl);

		KieHelper kieHelper = new KieHelper();
		kieHelper.addContent(drl, ResourceType.DRL);

		Results results = kieHelper.verify();

		if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
			List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
			for (Message message : messages) {
				System.out.println("Error: "+message.getText());
			}

			throw new IllegalStateException("Compilation errors were found. Check the logs.");
		}

		return kieHelper.build().newKieSession();
	}
}
