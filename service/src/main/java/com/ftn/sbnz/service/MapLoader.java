package com.ftn.sbnz.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.ftn.sbnz.model.models.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class MapLoader {
    public static Map loadMap(String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File(path), Map.class);
    }

    public static Map fromString(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(json, Map.class);
    }
}
