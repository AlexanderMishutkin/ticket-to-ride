package com.ftn.sbnz.service.tests;

import com.ftn.sbnz.model.models.*;
import com.ftn.sbnz.model.models.Card;
import com.ftn.sbnz.model.events.DangerousEvent;
import com.ftn.sbnz.service.MapLoader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.drools.core.time.SessionPseudoClock;
import org.drools.template.ObjectDataCompiler;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class TestAllRules {
    private KieServices ks;
    private KieContainer kContainer;
    private KieSession ksession;
    private KieSession ksession2;

    public void setUpBackward() throws IOException {
        this.ks = KieServices.Factory.get();
        this.kContainer = ks.getKieClasspathContainer();
        this.ksession = kContainer.newKieSession("bwKsession");

        InputStream template = new FileInputStream("D:/source/repos/java/ticket-to-ride/kjar/src/main/resources/rules/backward/backward.drt");
        InputStream data = new FileInputStream("D:/source/repos/java/ticket-to-ride/kjar/src/main/resources/data/scoring.csv");
        Reader dataReader = new InputStreamReader(data);

        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(dataReader);
        ArrayList<HashMap<String, Object>> data1 = new ArrayList<>();

        for (CSVRecord record : records) {
            HashMap<String, Object> row = new HashMap<>();
            for (String header : record.toMap().keySet()) {
                row.put(header, record.get(header));
            }
            data1.add(row);
        }

        ObjectDataCompiler compiler = new ObjectDataCompiler();
        System.out.println(data1);
        System.out.println(template);
        String drl = compiler.compile(data1, template);

        System.out.println(drl);

        ksession2 = this.createKieSessionFromDRL(drl);
    }

    @Test
    public void testGameFlowTakeCard() throws IOException {
        this.setUpBackward();

        Game game = new Game();
        Step currentStep = new Step();
        currentStep.setType(Step.Type.TAKE_FIRST_OPEN_CARD);
        game.setCurrentStep(currentStep);
        this.ksession.insert(game);
        this.ksession.fireAllRules();

        Step step1 = new Step();
        step1.setType(Step.Type.TAKE_SECOND_OPEN_CARD);
        Step step2 = new Step();
        step2.setType(Step.Type.TAKE_SECOND_CLOSED_CARD);

        assertThat(game.getPossibleSteps().contains(step1), equalTo(true));
        assertThat(game.getPossibleSteps().contains(step2), equalTo(true));
        this.ksession.destroy();
    }

    @Test
    public void testGameFlowTakeTasks() throws IOException {
        this.setUpBackward();

        Map map = MapLoader.loadMap("src/main/resources/map.json");

        Game game = new Game();
        game.setMap(map);

        Step currentStep = new Step();
        currentStep.setType(Step.Type.TAKE_THREE_TASKS);
        currentStep.setRelatedTasks(
                List.of(
                        new Task(1, map.getCities().get(0), map.getCities().get(1)),
                        new Task(2, map.getCities().get(1), map.getCities().get(2)),
                        new Task(3, map.getCities().get(2), map.getCities().get(3))
                )
        );
        game.setCurrentStep(currentStep);
        this.ksession.insert(game);
        this.ksession.fireAllRules();

        Step step1 = new Step();
        step1.setType(Step.Type.RETURN_TASKS);


        assertThat(game.getPossibleSteps().contains(step1), equalTo(true));
        this.ksession.destroy();
    }

    @Test
    public void testPotentialBuildStations() throws IOException {
        this.setUpBackward();

        Map map = MapLoader.loadMap("src/main/resources/map.json");
        map.setStations(
                List.of(
                        new Station(map.getCities().get(0), new User("b", "b"))
                )
        );

        Game game = new Game();
        game.setMap(map);
        game.setCurrentUser(new User("a", "a"));
        game.getCurrentUser().setCards(
                List.of(
                    new Card(1)
                )
        );

        Step currentStep = new Step();
        currentStep.setType(Step.Type.NOT_STARTED);
        game.setCurrentStep(currentStep);
        this.ksession.insert(game);
        this.ksession.fireAllRules();

        Step step1 = new Step();
        step1.setType(Step.Type.BUILD_STATION);
        step1.setRelatedCity(map.getCities().get(0));
        step1.setRelatedCards(List.of(new Card(1)));

        Step step2 = new Step();
        step2.setType(Step.Type.BUILD_STATION);
        step2.setRelatedCity(map.getCities().get(1));
        step2.setRelatedCards(List.of(new Card(1)));

        Step step3 = new Step();
        step3.setType(Step.Type.BUILD_STATION);
        step3.setRelatedCity(map.getCities().get(1));
        step3.setRelatedCards(List.of(new Card(2)));

        assertThat(game.getPossibleSteps().contains(step1), equalTo(false));
        assertThat(game.getPossibleSteps().contains(step2), equalTo(true));
        assertThat(game.getPossibleSteps().contains(step3), equalTo(false));
        this.ksession.destroy();
    }

    @Test
    public void testScoringEvent() throws IOException {
        this.setUpBackward();

        User user = new User("a", "a");
        User user2 = new User("b", "b");

        Map map = MapLoader.loadMap("src/main/resources/map.json");
        map.setStations(
                List.of(
                        new Station(map.getCities().get(0), user)
                )
        );
        map.setStretches(
                List.of(
                        new Stretch(
                                map.getPotentialStretches().get(1),
                                user
                        ),
                        new Stretch(
                                map.getPotentialStretches().get(0),
                                user2
                        ),
                        new Stretch(
                                map.getPotentialStretches().get(2),
                                user2
                        )
                )
        );
        user.setTasks(
                List.of(
                        new Task(4, map.getCities().get(2), map.getCities().get(5)),
                        new Task(1, map.getCities().get(3), map.getCities().get(5))
                )
        );
        user2.setTasks(
                List.of(
                        new Task(2, map.getCities().get(1), map.getCities().get(4)),
                        new Task(3, map.getCities().get(0), map.getCities().get(3))
                )
        );

        Game game = new Game();
        game.setMap(map);
        game.setCurrentUser(new User("a", "a"));
        game.getCurrentUser().setCards(
                List.of(
                    new Card(1)
                )
        );


        this.ksession.destroy();
    }

    @Test
    public void testBuildScoring() throws IOException {
        this.setUpBackward();

        User user = new User("a", "a");
        Map map = MapLoader.loadMap("src/main/resources/map.json");

        Game game = new Game();
        game.setMap(map);
        game.setCurrentUser(new User("a", "a"));
        game.setCurrentStep(
                new Step(
                        List.of(),
                        null,
                        new Stretch(
                                map.getPotentialStretches().get(0),
                                user
                        ),
                        List.of(),
                        Step.Type.BUILD_STRETCH
                )
        );

        this.ksession2.insert(game);
        this.ksession2.fireAllRules();

        assertThat(game.getCurrentUser().getScore(), equalTo(7));

        this.ksession2.destroy();
    }

    private KieSession createKieSessionFromDRL(String drl){
        System.out.println(drl);

        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kieHelper.build().newKieSession();
    }
}
